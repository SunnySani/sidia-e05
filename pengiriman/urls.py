from django.urls import path

from . import views

app_name = 'pengiriman'

urlpatterns = [
    path('kendaraan/', views.kendaraan, name='kendaraan'),
    path('kendaraan/add/', views.add_kendaraan, name='add_kendaraan'),
    path('kendaraan/update/<str:nomor_kendaraan>', views.update_kendaraan, name='update_kendaraan'),
    path('kendaraan/delete/<str:nomor_kendaraan>', views.delete_kendaraan, name='delete_kendaraan'),
    path('batch/', views.batch, name='batch'),
    path('batch/add/<str:nomor_transaksi>', views.add_batch, name='add_batch'),
    path('batch/detail/<int:no_transaksi>', views.detail_batch, name='detail_batch'),
    path('lokasi/', views.lokasi, name='lokasi'),
    path('lokasi/add/', views.add_lokasi, name='add_lokasi'),
    path('lokasi/update/<str:lokasi_id>', views.update_lokasi, name='update_lokasi'),
]