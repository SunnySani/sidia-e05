from django.shortcuts import render, redirect
from django.db import IntegrityError, InternalError, connection
from django.contrib import messages

import random, string

# Create your views here.

def kendaraan(request):
    with connection.cursor() as cursor:

        cursor.execute(f"""
            SELECT * FROM kendaraan
        """)

        context = {"kendaraans" : cursor.fetchall()}

    return render(request, "pengiriman/kendaraan.html", context)

def add_kendaraan(request):
    if request.session["role"] == "admin":
        if request.method == "POST":

            try:
                with connection.cursor() as cursor:

                    cursor.execute(f"""
                        INSERT INTO kendaraan VALUES 
                        ('{request.POST['nomor']}',
                        '{request.POST['nama']}',
                        '{request.POST['jenis']}',
                        '{request.POST['berat']}')
                    """)

                    return redirect("pengiriman:kendaraan")
            
            except IntegrityError:
                messages.add_message(request, messages.WARNING, f"Kendaraan dengan nomor {request.POST['nomor']} sudah terdaftar")

        return render(request, "pengiriman/kendaraan_add.html")

    else:
        return redirect("pengguna:wrong_role")

def update_kendaraan(request, nomor_kendaraan):
    if request.session["role"] == "admin":
        if request.method == "POST":
            with connection.cursor() as cursor:

                cursor.execute(f"""
                    UPDATE kendaraan SET
                    nomor='{request.POST['nomor']}',
                    nama='{request.POST['nama']}',
                    jenis_kendaraan='{request.POST['jenis']}',
                    berat_maksimum='{request.POST['berat']}'
                    WHERE nomor='{nomor_kendaraan}'
                """)

                return redirect("pengiriman:kendaraan")
        
        else:
            with connection.cursor() as cursor:

                cursor.execute(f"""
                    SELECT * FROM kendaraan
                    WHERE nomor='{nomor_kendaraan}'
                """)

                context = {"kendaraan" : cursor.fetchall()[0]}

            return render(request, "pengiriman/kendaraan_update.html", context)
    
    else:
        return redirect("pengguna:wrong_role")

def delete_kendaraan(request, nomor_kendaraan):
    if request.session["role"] == "admin":
        with connection.cursor() as cursor:

            cursor.execute(f"""
                DELETE FROM kendaraan
                WHERE nomor='{nomor_kendaraan}'
            """)

            return redirect("pengiriman:kendaraan")

    else:
        return redirect("pengguna:wrong_role")

def batch(request):

    username = request.session["username"]
    role = request.session["role"]

    context = {'role': role}

    if role == "admin":
        with connection.cursor() as cursor:

            cursor.execute(f"""
                SELECT psd.no_transaksi_sumber_daya, psd.username_petugas_faskes, f.kode_faskes_nasional, tsd.total_berat, rsp.status_permohonan, sb.status_batch
                FROM permohonan_sumber_daya_faskes AS psd
                NATURAL JOIN (
                    SELECT DISTINCT ON (nomor_permohonan)
                    nomor_permohonan AS no_transaksi_sumber_daya, kode_status_permohonan AS status_permohonan
                    FROM riwayat_status_permohonan
                    ORDER BY nomor_permohonan, tanggal DESC
                    ) AS rsp
                INNER JOIN transaksi_sumber_daya AS tsd ON tsd.nomor=psd.no_transaksi_sumber_daya
                INNER JOIN faskes AS f ON f.username_petugas=psd.username_petugas_faskes
                LEFT JOIN (
                    SELECT nomor_transaksi_sumber_daya, sp.status_batch
                    FROM batch_pengiriman
                    NATURAL JOIN (
                        SELECT DISTINCT ON (kode_batch)
                        kode_batch AS kode, kode_status_batch_pengiriman AS status_batch
                        FROM riwayat_status_pengiriman
                        ORDER BY kode_batch, tanggal DESC
                        ) AS sp
                    ) AS sb ON sb.nomor_transaksi_sumber_daya=psd.no_transaksi_sumber_daya
                ORDER BY psd.no_transaksi_sumber_daya;
            """)

            context['permohonans'] = cursor.fetchall()

        return render(request, "pengiriman/batch.html", context)

    elif role == "faskes":
        with connection.cursor() as cursor:

            cursor.execute(f"""
                SELECT psd.no_transaksi_sumber_daya, psd.username_petugas_faskes, f.kode_faskes_nasional, tsd.total_berat, rsp.status_permohonan, sb.status_batch
                FROM (SELECT * FROM permohonan_sumber_daya_faskes as psd where psd.username_petugas_faskes='{username}')AS psd
                NATURAL JOIN (
                    SELECT DISTINCT ON (nomor_permohonan)
                    nomor_permohonan AS no_transaksi_sumber_daya, kode_status_permohonan AS status_permohonan
                    FROM riwayat_status_permohonan
                    ORDER BY nomor_permohonan, tanggal DESC
                    ) AS rsp
                INNER JOIN transaksi_sumber_daya AS tsd ON tsd.nomor=psd.no_transaksi_sumber_daya
                INNER JOIN faskes AS f ON f.username_petugas=psd.username_petugas_faskes
                LEFT JOIN (
                    SELECT nomor_transaksi_sumber_daya, sp.status_batch
                    FROM batch_pengiriman
                    NATURAL JOIN (
                        SELECT DISTINCT ON (kode_batch)
                        kode_batch AS kode, kode_status_batch_pengiriman AS status_batch
                        FROM riwayat_status_pengiriman
                        ORDER BY kode_batch, tanggal DESC
                        ) AS sp
                    ) AS sb ON sb.nomor_transaksi_sumber_daya=psd.no_transaksi_sumber_daya
                ORDER BY psd.no_transaksi_sumber_daya;
            """)

            context['permohonans'] = cursor.fetchall()

        return render(request, "pengiriman/batch.html", context)

    elif role == "distribusi":
        with connection.cursor() as cursor:

            cursor.execute(f"""
                SELECT psd.no_transaksi_sumber_daya, psd.username_petugas_faskes, f.kode_faskes_nasional, tsd.total_berat, rsp.status_permohonan, sb.status_batch
                FROM permohonan_sumber_daya_faskes AS psd
                NATURAL JOIN (
                    SELECT DISTINCT ON (nomor_permohonan)
                    nomor_permohonan AS no_transaksi_sumber_daya, kode_status_permohonan AS status_permohonan
                    FROM riwayat_status_permohonan
                    ORDER BY nomor_permohonan, tanggal DESC
                    ) AS rsp
                INNER JOIN transaksi_sumber_daya AS tsd ON tsd.nomor=psd.no_transaksi_sumber_daya
                INNER JOIN faskes AS f ON f.username_petugas=psd.username_petugas_faskes
                JOIN (
                    SELECT nomor_transaksi_sumber_daya, sp.status_batch
                    FROM (SELECT * from batch_pengiriman where username_petugas_distribusi='{username}') as batch_pengiriman
                    NATURAL JOIN (
                        SELECT DISTINCT ON (kode_batch)
                        kode_batch AS kode, kode_status_batch_pengiriman AS status_batch
                        FROM riwayat_status_pengiriman
                        ORDER BY kode_batch, tanggal DESC
                        ) AS sp
                    ) AS sb ON sb.nomor_transaksi_sumber_daya=psd.no_transaksi_sumber_daya
                ORDER BY psd.no_transaksi_sumber_daya;
            """)

            context['permohonans'] = cursor.fetchall()

        return render(request, "pengiriman/batch.html", context)

    else:
        return redirect("pengguna:wrong_role")

def add_batch(request, nomor_transaksi):
    if request.session["role"] == "admin":
        if request.method == "POST":

            with connection.cursor() as cursor:
                
                if request.POST['petugas'] != "none" and request.POST['kendaraan'] != "none":
                    if len(request.POST['kode']) == 5:
                        try:
                            # Generate tanda terima
                            tanda_terima = ''.join(random.choices(string.ascii_letters + string.digits, k=30))

                            cursor.execute(f"""
                                INSERT INTO batch_pengiriman(kode,username_satgas,username_petugas_distribusi,
                                tanda_terima,nomor_transaksi_sumber_daya,no_kendaraan) VALUES 
                                ('{request.POST['kode']}',
                                '{request.POST['admin']}',
                                '{request.POST['petugas']}',
                                '{tanda_terima}',
                                '{request.POST['nomor']}',
                                '{request.POST['kendaraan']}')
                            """)

                            messages.add_message(request, messages.SUCCESS, "Batch pengiriman berhasil ditambahkan")

                        except InternalError:
                            messages.add_message(request, messages.WARNING, "Batch pengiriman untuk permohonan sumber daya ini sudah cukup")

                        except:
                            messages.add_message(request, messages.WARNING, "Terjadi masalah saat menambahkan batch pengiriman")

                    else:
                        messages.add_message(request, messages.WARNING, "Jumlah batch pengiriman sudah melewati batas maksimal sistem")

                else:
                    messages.add_message(request, messages.WARNING, "Terdapat data yang belum diisi, silakan lengkapi data terlebih dahulu")

        with connection.cursor() as cursor:

            # Generate kode batch (mungkin bisa dikasih batasan)
            cursor.execute(f"""
                SELECT kode FROM batch_pengiriman
                ORDER BY kode DESC
            """)

            row = cursor.fetchall()

            if len(row) == 0:
                kodeBatch = "B0000"
            else:
                kodeBatch = row[0][0]
                kodeBatch = kodeBatch[0] + str(int(kodeBatch[1:]) + 1)
                kodeBatch = kodeBatch[0] + "0" * (5 - len(kodeBatch)) + kodeBatch[1:]

            context = {"nomor_transaksi" : nomor_transaksi, "kode_batch" : kodeBatch}

            cursor.execute(f"""
                SELECT * FROM batch_pengiriman;
            """)

            context["pengirimans"] = cursor.fetchall()

            cursor.execute(f"""
                SELECT * FROM kendaraan
                WHERE nomor NOT IN (
                    SELECT no_kendaraan FROM batch_pengiriman
                    NATURAL JOIN (
                        SELECT DISTINCT ON (kode_batch)
                        kode_batch AS kode, kode_status_batch_pengiriman
                        FROM riwayat_status_pengiriman
                        ORDER BY kode_batch, tanggal DESC
                    ) AS rsp
                    WHERE rsp.kode_status_batch_pengiriman='PRO'
                    OR rsp.kode_status_batch_pengiriman='OTW'
                    OR nomor_transaksi_sumber_daya={nomor_transaksi}
                );
            """)

            context["kendaraans"] = cursor.fetchall()

            cursor.execute(f"""
                SELECT username, nama, no_sim FROM petugas_distribusi
                NATURAL JOIN pengguna
                WHERE username NOT IN (
                    SELECT username_petugas_distribusi FROM batch_pengiriman
                    NATURAL JOIN (
                        SELECT DISTINCT ON (kode_batch)
                        kode_batch AS kode, kode_status_batch_pengiriman
                        FROM riwayat_status_pengiriman
                        ORDER BY kode_batch, tanggal DESC
                    ) AS rsp
                    WHERE rsp.kode_status_batch_pengiriman='PRO'
                    OR rsp.kode_status_batch_pengiriman='OTW'
                    OR nomor_transaksi_sumber_daya={nomor_transaksi}
                );
            """)

            context["distributors"] = cursor.fetchall()

            cursor.execute(f"""
                SELECT bp.kode, k.nama, p.nama, la.alamat, lt.alamat
                FROM batch_pengiriman AS bp
                NATURAL JOIN (SELECT nomor AS no_kendaraan, nama FROM kendaraan) AS k
                INNER JOIN (SELECT username, nama FROM pengguna) AS p ON p.username=bp.username_petugas_distribusi
                INNER JOIN (SELECT id, CONCAT(
                    jalan_no, ', Kelurahan ', kelurahan, ', Kecamatan ', kecamatan, ', ', kabkot, ', ', provinsi
                ) AS alamat FROM lokasi) AS la ON la.id=bp.id_lokasi_asal
                INNER JOIN (SELECT id, CONCAT(
                    jalan_no, ', Kelurahan ', kelurahan, ', Kecamatan ', kecamatan, ', ', kabkot, ', ', provinsi
                ) AS alamat FROM lokasi) AS lt ON lt.id=bp.id_lokasi_tujuan;
            """)

            context["pengirimans"] = cursor.fetchall()

        return render(request, "pengiriman/batch_add.html", context)

    else:
        return redirect("pengguna:wrong_role")

def detail_batch(request, no_transaksi):

    context = {}

    with connection.cursor() as cursor:
        cursor.execute(f"""
                SELECT bp.kode, k.nama, p.nama, la.alamat, lt.alamat
                FROM (SELECT * FROM batch_pengiriman AS bp WHERE bp.nomor_transaksi_sumber_daya = {no_transaksi}) as bp
                    NATURAL JOIN (SELECT nomor AS no_kendaraan, nama FROM kendaraan) AS k
                    INNER JOIN (SELECT username, nama FROM pengguna) AS p ON p.username=bp.username_petugas_distribusi
                    INNER JOIN (SELECT id, CONCAT(
                        jalan_no, ', Kelurahan ', kelurahan, ', Kecamatan ', kecamatan, ', ', kabkot, ', ', provinsi
                    ) AS alamat FROM lokasi) AS la ON la.id=bp.id_lokasi_asal
                    INNER JOIN (SELECT id, CONCAT(
                        jalan_no, ', Kelurahan ', kelurahan, ', Kecamatan ', kecamatan, ', ', kabkot, ', ', provinsi
                    ) AS alamat FROM lokasi) AS lt ON lt.id=bp.id_lokasi_tujuan
            """)
        
        data = []
        for i in cursor:
            data.append(i)

        context['no_transaksi'] = no_transaksi
        context['data'] = data

    return render(request, 'pengiriman/batch_detail.html', context)

def lokasi(request):
    if request.session["role"] == "admin":
        with connection.cursor() as cursor:

            cursor.execute(f"""
                SELECT * FROM lokasi
            """)

            context = {"lokasis" : cursor.fetchall()}

        return render(request, "pengiriman/lokasi.html", context)

    else:
        return redirect("pengguna:wrong_role")

def add_lokasi(request):
    if request.session["role"] == "admin":
        if request.method == "POST":
            with connection.cursor() as cursor:

                cursor.execute(f"""
                    INSERT INTO lokasi VALUES 
                    ('{request.POST['id']}',
                    '{request.POST['provinsi']}',
                    '{request.POST['kabkot']}',
                    '{request.POST['kecamatan']}',
                    '{request.POST['kelurahan']}',
                    '{request.POST['jalan']}')
                """)

            return redirect("pengiriman:lokasi")

        else:
            with connection.cursor() as cursor:

                # Generate id lokasi (mungkin bisa dikasih batasan)
                cursor.execute(f"""
                    SELECT id FROM lokasi
                    ORDER BY id DESC
                """)

                row = cursor.fetchall()

                if len(row) == 0:
                    lokasiId = "Loc00"
                else:
                    lokasiId = row[0][0]
                    lokasiId = lokasiId[0:3] + str(int(lokasiId[3:5]) + 1)
                    lokasiId = lokasiId if len(lokasiId) == 5 else lokasiId[0:3] + "0" + lokasiId[3]

                context = {"id_lokasi" : lokasiId}
            
            return render(request, "pengiriman/lokasi_add.html", context)

    else:
        return redirect("pengguna:wrong_role")

def update_lokasi(request, lokasi_id):
    if request.session["role"] == "admin":
        if request.method == "POST":
            with connection.cursor() as cursor:

                cursor.execute(f"""
                    UPDATE lokasi SET
                    provinsi='{request.POST['provinsi']}',
                    kabkot='{request.POST['kabkot']}',
                    kecamatan='{request.POST['kecamatan']}',
                    kelurahan='{request.POST['kelurahan']}',
                    jalan_no='{request.POST['jalan']}'
                    WHERE id='{lokasi_id}'
                """)

                return redirect("pengiriman:lokasi")
        
        else:
            with connection.cursor() as cursor:

                cursor.execute(f"""
                    SELECT * FROM lokasi
                    WHERE id='{lokasi_id}'
                """)

                context = {"lokasi" : cursor.fetchall()[0]}

            return render(request, "pengiriman/lokasi_update.html", context)
    
    else:
        return redirect("pengguna:wrong_role")