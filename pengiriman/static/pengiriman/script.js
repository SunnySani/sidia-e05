$(document).ready( function() {
    $('.pilih-kendaraan').click( function() {
        console.log("Memilih kendaraan dengan nomor " + $(this).val());

        $('#form-kendaraan').text("Kendaraan: " + $(this).parent().parent().children('td').eq(1).html());
        $('#input-kendaraan').val($(this).val());

        $('#pilih-kendaraan-modal').modal('hide');
    });

    $('.pilih-petugas-distribusi').click( function() {
        console.log("Memilih kendaraan dengan nomor " + $(this).val());

        $('#form-petugas-distribusi').text("Petugas Distribusi: " + $(this).parent().parent().children('td').eq(0).html());
        $('#input-petugas-distribusi').val($(this).val());

        $('#pilih-petugas-distribusi-modal').modal('hide');
    });
});