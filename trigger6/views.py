from django.shortcuts import render, redirect
from django.db import connection
from django.contrib import messages

def WarehouseStockCreate(request):

    context = {}

    with connection.cursor() as cursor:
        cursor.execute(f"""
        SELECT * FROM WAREHOUSE_PROVINSI
        """)
        sql_data = cursor.fetchall()
        warehouse = []
        for i in sql_data:
            warehouse.append(i[0])
        context["warehouse"] = warehouse
    
    with connection.cursor() as cursor:
            cursor.execute(f"""
            SELECT kode FROM ITEM_SUMBER_DAYA
            """)
            sql_data = cursor.fetchall()
            item = []
            for i in sql_data:
                item.append(i[0])
            context["item"] = item

    if request.method == "POST" and request.POST['button'] == "check":
        kode_warehouse = request.POST['warehouse']
        kode_item = request.POST['item']

        check_warehouse = ""
        check_item = ""
        with connection.cursor() as cursor:
            cursor.execute(f"""
            SELECT provinsi, kabkot, kecamatan, kelurahan, jalan_no
            FROM STOK_WAREHOUSE_PROVINSI AS swp, LOKASI AS l
            WHERE swp.id_lokasi_warehouse=l.id AND swp.id_lokasi_warehouse='{kode_warehouse}'
            """)
            for i in cursor.fetchall():
                check_warehouse += i[0] + " - " + i[1] + " - " + i[2] + " - " + i[3] + " - " + i[4]
            
            cursor.execute(f"""
            SELECT *
            FROM ITEM_SUMBER_DAYA AS isd
            WHERE isd.kode='{kode_item}'
            """)
            for i in cursor.fetchall():
                check_item += str(i[0]) + " - " + str(i[1]) + " - " + str(i[2]) + " - " + str(i[3]) + " - " + str(i[4]) + " - " + str(i[5])

        context['check_warehouse'] = check_warehouse
        context['check_item'] = check_item
        context['kode_warehouse'] = kode_warehouse
        context['kode_item'] = kode_item

    if request.method == "POST" and request.POST['button'] == "submit":
        kode_warehouse = request.POST['warehouse']
        kode_item = request.POST['item']
        jumlah = request.POST['jumlah']

        if not kode_warehouse or not kode_item:
            messages.add_message(request, messages.WARNING, f"Informasi tidak lengkap")
            return redirect("trigger6:WarehouseStockCreate")

        with connection.cursor() as cursor:
            cursor.execute(f"""
            INSERT INTO STOK_WAREHOUSE_PROVINSI
            VALUES ('{kode_warehouse}', '{kode_item}', {jumlah})
            """)

        print("Insert Stock Baru: " + kode_item, kode_warehouse, jumlah)
        messages.add_message(request, messages.SUCCESS, f"Penambahan Stock Warehouse Berhasil")
        return redirect("trigger6:WarehouseStockCreate")

    return render(request, "trigger6/admin/WarehouseStockCreate.html", context)


def WarehouseStockRead(request):

    context = {}
    stock_data = {}

    with connection.cursor() as cursor:
        cursor.execute(f"""
        SELECT * FROM STOK_WAREHOUSE_PROVINSI
        """)
        sql_data = cursor.fetchall()
        for i in sql_data:
            if i[0] not in stock_data.keys():
                stock_data[i[0]] = [[i[1], i[2]]]
            else:
                stock_data[i[0]].append([i[1], i[2]])
    
    context["stock_data"] = stock_data

    return render(request, "trigger6/admin/WarehouseStockRead.html", context)

def WarehouseStockUpdate(request, warehouse, item):
    
    if request.method == "POST" and request.POST['button'] == "submit":
        jumlah = request.POST['jumlah']
        with connection.cursor() as cursor:
            cursor.execute(f"""
            UPDATE STOK_WAREHOUSE_PROVINSI
            SET jumlah = {jumlah}
            WHERE id_lokasi_warehouse = '{warehouse}' AND kode_item_sumber_daya = '{item}'
            """)
        messages.add_message(request, messages.SUCCESS, f"Update Berhasil")
        return redirect("trigger6:WarehouseStockRead")
    
    check_warehouse = ""
    check_item = ""
    with connection.cursor() as cursor:
        cursor.execute(f"""
        SELECT provinsi, kabkot, kecamatan, kelurahan, jalan_no
            FROM STOK_WAREHOUSE_PROVINSI AS swp, LOKASI AS l
            WHERE swp.id_lokasi_warehouse=l.id AND swp.id_lokasi_warehouse='{warehouse}'
        """)
        for i in cursor.fetchall():
            check_warehouse += i[0] + " - " + i[1] + " - " + i[2] + " - " + i[3] + " - " + i[4]
            
        cursor.execute(f"""
            SELECT *
            FROM ITEM_SUMBER_DAYA AS isd
            WHERE isd.kode='{item}'
        """)
        for i in cursor.fetchall():
            check_item += str(i[0]) + " - " + str(i[1]) + " - " + str(i[2]) + " - " + str(i[3]) + " - " + str(i[4]) + " - " + str(i[5])
    
    context = {
        'warehouse': warehouse,
        'item': item,
        'check_warehouse': check_warehouse,
        'check_item': check_item
    }

    return render(request, "trigger6/admin/WarehouseStockUpdate.html", context)

def WarehouseStockReadFaskes(request):

    username = request.session["username"];
    context = {}
    stock_data = {}

    with connection.cursor() as cursor:
        cursor.execute(f"""
        SELECT id_lokasi_warehouse, kode_item_sumber_daya, jumlah
        FROM (SELECT alamat_prov FROM PENGGUNA WHERE username='{username}') AS user_data,
             (SELECT * FROM STOK_WAREHOUSE_PROVINSI AS swp, lokasi AS l WHERE swp.id_lokasi_warehouse=l.id) AS stock_and_location
        WHERE stock_and_location.provinsi=user_data.alamat_prov;
        """)
        sql_data = cursor.fetchall()
        for i in sql_data:
            if i[0] not in stock_data.keys():
                stock_data[i[0]] = [[i[1], i[2]]]
            else:
                stock_data[i[0]].append([i[1], i[2]])
    
    context["stock_data"] = stock_data

    return render(request, "trigger6/petugas_faskes/WarehouseStockRead.html", context)

def RiwayatStatusBatchCreate(request):

    if request.method == "POST" and request.POST['button'] == "submit":
        status = kode_item = request.POST['status']
        
        collected_data = []
        converted_data = []
        if status == 'OTW':
            with connection.cursor() as cursor:
                cursor.execute(f"""
                SELECT kode_batch
                FROM RIWAYAT_STATUS_PENGIRIMAN
                WHERE kode_status_batch_pengiriman='PRO'
                """)
                sql_data = cursor.fetchall()
                for i in sql_data:
                    collected_data.append(i[0])

                cursor.execute(f"""
                SELECT kode_batch
                FROM RIWAYAT_STATUS_PENGIRIMAN
                WHERE kode_status_batch_pengiriman='OTW'
                """)
                sql_data = cursor.fetchall()
                for i in sql_data:
                    converted_data.append(i[0])
                
                    
                for i in collected_data:
                    if i not in converted_data:
                        cursor.execute(f"""
                        INSERT INTO RIWAYAT_STATUS_PENGIRIMAN VALUES
                        ('OTW', {i}, current_timestamp) 
                        """)
            messages.add_message(request, messages.SUCCESS, f"Berhasil Membuat Riwayat Pengiriman Baru")
            redirect('trigger6:RiwayatStatusBatchRead')

        elif status == 'DLV' or status == 'HLG' or status == 'RET':
            print("MASUK ELIF")
            with connection.cursor() as cursor:
                cursor.execute(f"""
                SELECT kode_batch
                FROM RIWAYAT_STATUS_PENGIRIMAN
                WHERE kode_status_batch_pengiriman='OTW'
                """)
                sql_data = cursor.fetchall()
                for i in sql_data:
                    collected_data.append(i[0])

                cursor.execute(f"""
                SELECT kode_batch
                FROM RIWAYAT_STATUS_PENGIRIMAN
                WHERE kode_status_batch_pengiriman='DLV'
                      OR kode_status_batch_pengiriman='HLG'
                      OR kode_status_batch_pengiriman='RET'
                """)
                sql_data = cursor.fetchall()
                for i in sql_data:
                    converted_data.append(i[0])
                    
                for i in collected_data:
                    if i not in converted_data:
                        cursor.execute(f"""
                        INSERT INTO RIWAYAT_STATUS_PENGIRIMAN VALUES
                        ('{status}', '{i}', current_timestamp) 
                        """)
            messages.add_message(request, messages.SUCCESS, f"Berhasil Membuat Riwayat Pengiriman Baru")
            redirect('trigger6:RiwayatStatusBatchRead')
    
    return render(request, "trigger6/admin/RiwayatStatusBatchCreate.html")

def RiwayatStatusBatchRead(request):

    context = {}
    riwayat_data = {}
    role = request.session["role"]
    username = request.session["username"]
    
    with connection.cursor() as cursor:
        if (role == 'admin'):
            cursor.execute(f"""
            SELECT bp.nomor_transaksi_sumber_daya, riwayat.kode, riwayat.nama, bp.username_satgas, riwayat.tanggal
            FROM (SELECT sbp.kode, sbp.nama, rsp.tanggal, rsp.kode_batch
                FROM RIWAYAT_STATUS_PENGIRIMAN AS rsp, STATUS_BATCH_PENGIRIMAN AS sbp
                WHERE rsp.kode_status_batch_pengiriman=sbp.kode) AS riwayat,
                BATCH_PENGIRIMAN AS bp
            WHERE bp.kode=riwayat.kode_batch
            """)
        elif (role =='distribusi'):
            cursor.execute(f"""
            SELECT bp.nomor_transaksi_sumber_daya, riwayat.kode, riwayat.nama, bp.username_satgas, riwayat.tanggal
            FROM (SELECT sbp.kode, sbp.nama, rsp.tanggal, rsp.kode_batch
                  FROM RIWAYAT_STATUS_PENGIRIMAN AS rsp, STATUS_BATCH_PENGIRIMAN AS sbp
                  WHERE rsp.kode_status_batch_pengiriman=sbp.kode) AS riwayat,
                 (SELECT * FROM BATCH_PENGIRIMAN where username_petugas_distribusi='{username}') AS bp
            WHERE bp.kode=riwayat.kode_batch
            """)
        sql_data = cursor.fetchall()
        for i in sql_data:
            if i[0] not in riwayat_data.keys():
                riwayat_data[i[0]] = [[str(1), i[1], i[2], i[3], i[4]]]
            else:
                number = len(riwayat_data[i[0]])+1
                riwayat_data[i[0]].append([str(number), i[1], i[2], i[3], i[4]])
    
    print(riwayat_data)
    
    context["riwayat_data"] = riwayat_data

    return render(request, "trigger6/RiwayatStatusBatchRead.html", context)
