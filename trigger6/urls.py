from django.urls import path

from . import views

app_name = 'trigger6'

urlpatterns = [
    path('WarehouseStockCreate', views.WarehouseStockCreate, name = 'WarehouseStockCreate'),
    path('WarehouseStockRead', views.WarehouseStockRead, name = 'WarehouseStockRead'),
    path('WarehouseStockUpdate/<str:warehouse>/<str:item>', views.WarehouseStockUpdate, name = 'WarehouseStockUpdate'),
    path('WarehouseStockReadFaskes', views.WarehouseStockReadFaskes, name = 'WarehouseStockReadFaskes'),

    path('RiwayatStatusBatchCreate', views.RiwayatStatusBatchCreate, name = 'RiwayatStatusBatchCreate'),
    path('RiwayatStatusBatchRead', views.RiwayatStatusBatchRead, name = 'RiwayatStatusBatchRead')
]