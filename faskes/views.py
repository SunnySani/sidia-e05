from django.shortcuts import render, redirect
from django.db import IntegrityError, InternalError, OperationalError, connection
from django.contrib import messages

# Create your views here.

def index(request):
    if request.session["role"] == "admin" or request.session["role"] == "supplier":
        with connection.cursor() as cursor:

            cursor.execute(f"""
                SELECT kode_faskes_nasional, CONCAT_WS(', ', jalan_no, kelurahan, kecamatan, kabkot, provinsi) as lokasi, nama, nama_tipe
                FROM faskes f JOIN lokasi l ON l.id = f.id_lokasi 
                JOIN tipe_faskes tf ON tf.kode = f.kode_tipe_faskes
                JOIN pengguna p ON f.username_petugas = p.username
            """)

            context = {"faskeses" : cursor.fetchall()}
    else:
        context = {}

    return render(request, "faskes/index.html", context)

def add_faskes(request):
    if request.session["role"] == "admin":
        if request.method == "POST":
            if (request.POST.get('kode_faskes') is None or request.POST.get('id_lokasi') is None or request.POST.get('username_petugas') is None or request.POST.get('kode_tipe_faskes') is None):
                messages.add_message(request, messages.WARNING, "Isi permintaan penambahan tidak valid")
            if (request.POST['kode_faskes'] == "" or request.POST['id_lokasi'] == "" or request.POST['username_petugas'] == "" or request.POST['kode_tipe_faskes'] == ""):
                messages.add_message(request, messages.WARNING, "Terdapat  data  yang  belum  diisi,  silahkan  lengkapi  data terlebih dahulu")
            else :
                try:
                    with connection.cursor() as cursor:

                        cursor.execute(f"""
                            INSERT INTO faskes VALUES 
                            ('{request.POST['kode_faskes']}',
                            '{request.POST['id_lokasi']}',
                            '{request.POST['username_petugas']}',
                            '{request.POST['kode_tipe_faskes']}')
                        """)

                        messages.add_message(request, messages.SUCCESS, "Data Fasilitas Kesehatan Telah Disimpan!")

                        return redirect("faskes:index")
                except IntegrityError:
                    messages.add_message(request, messages.WARNING, f"Data Fasilitas Kesehatan dengan kode {request.POST['kode_faskes']} sudah terdaftar")

        with connection.cursor() as cursor:

            cursor.execute(f"""
                SELECT id, CONCAT_WS(', ', jalan_no, kelurahan, kecamatan, kabkot, provinsi) as alamat
                FROM lokasi
            """)

            context = {"lokasis" : cursor.fetchall()}

            cursor.execute(f"""
                SELECT username, nama 
                FROM petugas_faskes NATURAL JOIN pengguna 
                WHERE username NOT IN (SELECT username_petugas FROM faskes)
            """)

            context["petugas_faskeses"] = cursor.fetchall()

            cursor.execute(f"""
                SELECT kode, nama_tipe FROM tipe_faskes
            """)

            context["tipe_faskeses"] = cursor.fetchall()

        return render(request, "faskes/addfaskes.html", context)
    elif request.session["role"] == "supplier":
        messages.add_message(request, messages.WARNING, f"Hanya admin yang dapat menambahkan data faskes")
        return redirect("faskes:index")
    else:
        return redirect("faskes:index")

def update_faskes(request):
    if request.session["role"] == "admin":
        if request.method == "POST":
            if (request.POST.get('kode_faskes') is None or request.POST.get('kode_faskes') == ""):
                messages.add_message(request, messages.WARNING, "Isi permintaan update tidak valid")
                redirect("faskes:index")
            elif (request.POST.get('id_lokasi') is None or request.POST.get('username_petugas') is None or request.POST.get('kode_tipe_faskes') is None):
                messages.add_message(request, messages.WARNING, "Isi permintaan update tidak valid")
            if (request.POST['id_lokasi'] == "" or request.POST['username_petugas'] == "" or request.POST['kode_tipe_faskes'] == ""):
                messages.add_message(request, messages.WARNING, "Terdapat  data  yang  belum  diisi,  silahkan  lengkapi  data terlebih dahulu")
            else :
                try:
                    with connection.cursor() as cursor:

                        cursor.execute(f"""
                            UPDATE faskes
                            SET id_lokasi = '{request.POST['id_lokasi']}',
                            username_petugas = '{request.POST['username_petugas']}',
                            kode_tipe_faskes = '{request.POST['kode_tipe_faskes']}'
                            WHERE kode_faskes_nasional = '{request.POST['kode_faskes']}'
                        """)

                        messages.add_message(request, messages.SUCCESS, "Data Fasilitas Kesehatan Telah Diupdate!")

                        return redirect("faskes:index")
                
                except OperationalError:
                    messages.add_message(request, messages.WARNING, f"Tidak ada data Fasilitas Kesehatan dengan kode {request.POST['kode_faskes']}")

        with connection.cursor() as cursor:

            cursor.execute(f"""
                SELECT kode_faskes_nasional, id_lokasi, username, nama, kode_tipe_faskes 
                FROM faskes f JOIN pengguna p ON p.username = f.username_petugas
                WHERE kode_faskes_nasional = '{request.GET['kode_faskes']}'
            """)

            data = cursor.fetchone()

            if (data is None):
                messages.add_message(request, messages.WARNING, f"Tidak ada data Fasilitas Kesehatan dengan kode {request.GET['kode_faskes']}")
                return redirect("faskes:index")

            context = {"current" : data}

            cursor.execute(f"""
                SELECT id, CONCAT_WS(', ', jalan_no, kelurahan, kecamatan, kabkot, provinsi) as alamat
                FROM lokasi
            """)

            context["lokasis"] = cursor.fetchall()

            cursor.execute(f"""
                SELECT username, nama 
                FROM petugas_faskes NATURAL JOIN pengguna 
                WHERE username NOT IN (SELECT username_petugas FROM faskes)
            """)

            context["petugas_faskeses"] = cursor.fetchall()

            cursor.execute(f"""
                SELECT kode, nama_tipe FROM tipe_faskes
            """)

            context["tipe_faskeses"] = cursor.fetchall()

        return render(request, "faskes/updatefaskes.html", context)
    elif request.session["role"] == "supplier":
        messages.add_message(request, messages.WARNING, f"Hanya admin yang dapat mengupdate data faskes")
        return redirect("faskes:index")
    else:
        return redirect("faskes:index")

def delete_faskes(request):
    if request.session["role"] == "admin":
        if request.method == "POST":
            if (request.POST.get('kode_faskes') is None or request.POST.get('kode_faskes') == ""):
                messages.add_message(request, messages.WARNING, "Isi permintaan penghapusan tidak valid")
            with connection.cursor() as cursor:

                cursor.execute(f"""
                    DELETE FROM faskes
                    WHERE kode_faskes_nasional = '{request.POST['kode_faskes']}'
                """)

                messages.add_message(request, messages.SUCCESS, "Data Fasilitas Kesehatan telah Dihapus!")
        
        else:
            messages.add_message(request, messages.WARNING, f"Metode Permintaan tidak valid")
        
        return redirect("faskes:index")
    elif request.session["role"] == "supplier":
        messages.add_message(request, messages.WARNING, f"Hanya admin yang dapat menghapus data faskes")
        return redirect("faskes:index")
    else:
        return redirect("faskes:index")