from django.urls import path

from . import views

app_name = 'faskes'

urlpatterns = [
    path('', views.index, name='index'),
    path('create', views.add_faskes, name='addfaskes'),
    path('update', views.update_faskes, name='updatefaskes'),
    path('delete', views.delete_faskes, name='deletefaskes'),
]