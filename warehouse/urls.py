from django.urls import path

from . import views

app_name = 'warehouse'

urlpatterns = [
    path('', views.index, name='index'),
    path('create', views.add_warehouse, name='addwarehousesatgas'),
    path('update', views.update_warehouse, name='updatewarehousesatgas'),
    path('delete', views.delete_warehouse, name='deletewarehousesatgas'),
]