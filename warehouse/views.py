from django.shortcuts import render, redirect
from django.db import IntegrityError, InternalError, OperationalError, connection
from django.contrib import messages

# Create your views here.

def index(request):
    if request.session["role"] == "admin" or request.session["role"] == "supplier":
        with connection.cursor() as cursor:

            cursor.execute(f"""
                SELECT row_number() over (order by (select null)), id_lokasi, provinsi, CONCAT_WS(', ', jalan_no, kelurahan, kecamatan, kabkot, provinsi) as lokasi
                FROM warehouse_provinsi wp JOIN lokasi l on l.id = wp.id_lokasi
            """)

            context = {"warehouses" : cursor.fetchall()}
    else:
        context = {}

    return render(request, "warehouse/index.html", context)

def add_warehouse(request):
    if request.session["role"] == "admin":
        if request.method == "POST":
            if (request.POST.get('provinsi') is None or request.POST.get('id_lokasi') is None):
                messages.add_message(request, messages.WARNING, "Isi permintaan penambahan tidak valid")
            elif (request.POST['provinsi'] == "" or request.POST['id_lokasi'] == ""):
                messages.add_message(request, messages.WARNING, "Terdapat  data  yang  belum  diisi,  silahkan  lengkapi  data terlebih dahulu")
            else :
                with connection.cursor() as cursor:

                    cursor.execute(f"""
                        SELECT provinsi
                        FROM lokasi
                        WHERE id = '{request.POST['id_lokasi']}'
                    """)

                    prov_lokasi = cursor.fetchone()

                    if (prov_lokasi is None):
                        messages.add_message(request, messages.WARNING, f"Data Lokasi dengan id lokasi {request.POST['id_lokasi']} tidak ditemukan")
                    elif (prov_lokasi[0] != request.POST['provinsi']):
                        messages.add_message(request, messages.WARNING, f"Data provinsi Lokasi dengan id lokasi {request.POST['id_lokasi']} tidak sesuai ({prov_lokasi[0]}, bukan {request.POST['id_lokasi']})")
                    else:
                        try:
                            with connection.cursor() as cursor:

                                cursor.execute(f"""
                                    INSERT INTO warehouse_provinsi VALUES 
                                    ('{request.POST['id_lokasi']}')
                                """)

                                messages.add_message(request, messages.SUCCESS, "Data Warehouse Satgas telah disimpan!")

                                return redirect("warehouse:index")
                        except IntegrityError:
                            messages.add_message(request, messages.WARNING, f"Data Warehouse Satgas dengan id lokasi {request.POST['id_lokasi']} sudah terdaftar")

        with connection.cursor() as cursor:

            cursor.execute(f"""
                SELECT DISTINCT provinsi 
                FROM lokasi 
                WHERE provinsi NOT IN
                (SELECT provinsi FROM warehouse_provinsi wp JOIN lokasi l ON wp.id_lokasi = l.id)
            """)

            context = {"provinsis" : cursor.fetchall()}

            cursor.execute(f"""
                SELECT id, provinsi, CONCAT_WS(', ', jalan_no, kelurahan, kecamatan, kabkot, provinsi) as alamat
                FROM lokasi
                WHERE provinsi NOT IN
                (SELECT provinsi FROM warehouse_provinsi wp JOIN lokasi l ON wp.id_lokasi = l.id)
            """)

            context["lokasis"] = cursor.fetchall()

        return render(request, "warehouse/addwarehousesatgas.html", context)
    elif request.session["role"] == "supplier":
        messages.add_message(request, messages.WARNING, f"Hanya admin yang dapat menambahkan data Warehouse Satgas")
        return redirect("warehouse:index")
    else:
        return redirect("warehouse:index")

def update_warehouse(request):
    if request.session["role"] == "admin":
        if request.method == "POST":
            if (request.POST.get('id_lokasi_before') is None or request.POST.get('id_lokasi_before') == ""):
                messages.add_message(request, messages.WARNING, "Isi permintaan update tidak valid")
                redirect("warehouse:index")
            elif (request.POST.get('provinsi') is None or request.POST.get('id_lokasi') is None):
                messages.add_message(request, messages.WARNING, "Isi permintaan update tidak valid")
            elif (request.POST['provinsi'] == "" or request.POST['id_lokasi'] == ""):
                messages.add_message(request, messages.WARNING, "Terdapat  data  yang  belum  diisi,  silahkan  lengkapi  data terlebih dahulu")
            else :
                with connection.cursor() as cursor:

                    cursor.execute(f"""
                        SELECT provinsi
                        FROM lokasi
                        WHERE id = '{request.POST['id_lokasi']}'
                    """)

                    prov_lokasi = cursor.fetchone()

                    if (prov_lokasi is None):
                        messages.add_message(request, messages.WARNING, f"Data Lokasi dengan id lokasi {request.POST['id_lokasi']} tidak ditemukan")
                    elif (prov_lokasi[0] != request.POST['provinsi']):
                        messages.add_message(request, messages.WARNING, f"Data provinsi Lokasi dengan id lokasi {request.POST['id_lokasi']} tidak sesuai ({prov_lokasi[0]}, bukan {request.POST['provinsi']})")
                    else:
                        try:
                            with connection.cursor() as cursor:

                                cursor.execute(f"""
                                    UPDATE warehouse_provinsi 
                                    SET id_lokasi = '{request.POST['id_lokasi']}'
                                    WHERE id_lokasi = '{ request.POST['id_lokasi_before'] }'
                                """)

                                messages.add_message(request, messages.SUCCESS, "Data Warehouse Satgas telah disimpan!")

                                return redirect("warehouse:index")
                        except IntegrityError:
                            messages.add_message(request, messages.WARNING, f"Data Warehouse Satgas dengan id lokasi {request.POST['id_lokasi']} sudah terdaftar")

            id_lokasi = request.POST['id_lokasi_before']
        else:
            id_lokasi = request.GET['id_lokasi']
        with connection.cursor() as cursor:

            cursor.execute(f"""
                SELECT provinsi, id_lokasi, CONCAT_WS(', ', jalan_no, kelurahan, kecamatan, kabkot, provinsi) as alamat
                FROM warehouse_provinsi wp JOIN lokasi l ON wp.id_lokasi = l.id
                WHERE id_lokasi = '{id_lokasi}'
            """)

            data = cursor.fetchone()

            if (data is None):
                messages.add_message(request, messages.WARNING, f"Tidak ada data Warehouse Satgas dengan id lokasi {request.GET['id_lokasi']}")
                return redirect("warehouse:index")

            context = {"current" : data}

            cursor.execute(f"""
                SELECT id, provinsi, CONCAT_WS(', ', jalan_no, kelurahan, kecamatan, kabkot, provinsi) as alamat
                FROM lokasi
                WHERE provinsi = '{data[0]}' AND id != '{id_lokasi}'
            """)

            context["currentlocs"] = cursor.fetchall()

            cursor.execute(f"""
                SELECT id, provinsi, CONCAT_WS(', ', jalan_no, kelurahan, kecamatan, kabkot, provinsi) as alamat
                FROM lokasi
            """)

            context["lokasis"] = cursor.fetchall()

        return render(request, "warehouse/updatewarehousesatgas.html", context)
    elif request.session["role"] == "supplier":
        messages.add_message(request, messages.WARNING, f"Hanya admin yang dapat mengupdate data Warehouse Satgas")
        return redirect("warehouse:index")
    else:
        return redirect("warehouse:index")

def delete_warehouse(request):
    if request.session["role"] == "admin":
        if request.method == "POST":
            if (request.POST.get('id_lokasi') is None or request.POST.get('id_lokasi') == ""):
                messages.add_message(request, messages.WARNING, "Isi permintaan penghapusan tidak valid")
            with connection.cursor() as cursor:

                cursor.execute(f"""
                    DELETE FROM warehouse_provinsi
                    WHERE id_lokasi = '{request.POST['id_lokasi']}'
                """)

                messages.add_message(request, messages.SUCCESS, "Data Warehouse Satgas telah Dihapus!")        
        else:
            messages.add_message(request, messages.WARNING, f"Metode Permintaan tidak valid")

        return redirect("warehouse:index")
    elif request.session["role"] == "supplier":
        messages.add_message(request, messages.WARNING, f"Hanya admin yang dapat menghapus data Warehouse Satgas")
        return redirect("warehouse:index")
    else:
        return redirect("warehouse:index")
